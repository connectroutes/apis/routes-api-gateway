module routes-api-gateway

go 1.12

require (
	github.com/go-redis/redis/v7 v7.0.0-beta.6
	github.com/golang/protobuf v1.3.2
	github.com/joho/godotenv v1.3.0
	github.com/mwitkow/grpc-proxy v0.0.0-20181017164139-0f1106ef9c76
	github.com/stretchr/testify v1.4.0
	golang.org/x/net v0.0.0-20190923162816-aa69164e4478
	google.golang.org/grpc v1.26.0
)
